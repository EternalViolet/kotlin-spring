package com.example.amazing

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AmazingApplication

fun main(args: Array<String>) {
    runApplication<AmazingApplication>(*args)
}
