package com.example.amazing.services

import com.example.amazing.models.Burger
import com.example.amazing.repositories.BurgerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class BurgerService {

    @Autowired
    lateinit var burgerRepository: BurgerRepository

    fun getBurger(burgerId: Long): Burger {
        return burgerRepository.findByIdOrNull(burgerId) ?: throw NoSuchElementException("burger does not exist")
    }

    fun getAllBurgers(): Iterable<Burger> {
        return burgerRepository.findAll()
    }

    fun insertBurger(burger: Burger): Burger {
        return burgerRepository.save(burger)
    }

    fun deleteBurger(burgerId: Long) {
        burgerRepository.deleteById(burgerId)
    }
}
