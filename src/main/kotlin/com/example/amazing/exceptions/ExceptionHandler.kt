package com.example.amazing.exceptions

import org.slf4j.LoggerFactory
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    private val log = LoggerFactory.getLogger(this::class.java)

    @ExceptionHandler(value = [EmptyResultDataAccessException::class])
    fun handleNoResult(ex: EmptyResultDataAccessException): ResponseEntity<Error> {
        log.info("Caught an exception", ex)
        val msg = with(ex.message) {
            when {
                this == null -> ""
                this.contains("No class com.example.amazing.models.Burger entity with id") -> "Burger doesn't exist!"
                else -> ""
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
            Error(message = msg)
        )
    }
}
