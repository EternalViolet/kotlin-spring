package com.example.amazing.exceptions

import java.time.Instant

data class Error(
    val timestamp: Instant = Instant.now(),
    val message: String
)
