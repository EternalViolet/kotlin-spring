package com.example.amazing.repositories

import com.example.amazing.models.Burger
import org.springframework.data.repository.CrudRepository

interface BurgerRepository : CrudRepository<Burger, Long>
