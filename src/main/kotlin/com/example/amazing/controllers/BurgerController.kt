package com.example.amazing.controllers

import com.example.amazing.models.Burger
import com.example.amazing.services.BurgerService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/burger")
class BurgerController {

    @Autowired
    lateinit var burgerService: BurgerService

    @GetMapping
    @Operation(
        responses = [
            ApiResponse(
                responseCode = "200",
                content = [Content(
                    array = ArraySchema(
                        schema = Schema(implementation = Burger::class)
                    )
                )]
            )
        ]
    )
    fun getBurgers(
        @RequestParam(name = "burger_id", required = false) burgerId: Long?
    ): ResponseEntity<Iterable<Burger>> {
        val burgers = if (burgerId == null) {
            burgerService.getAllBurgers()
        } else {
            try {
                listOf(burgerService.getBurger(burgerId))
            } catch (ex: NoSuchElementException) {
                listOf<Burger>()
            }
        }

        return ResponseEntity.ok(burgers)
    }

    @PostMapping
    @Operation(
        responses = [
            ApiResponse(
                responseCode = "201"
            )
        ]
    )
    fun createBurger(
        @RequestBody burger: Burger
    ): ResponseEntity<Long> {
        val savedBurger = burgerService.insertBurger(burger)
        return ResponseEntity.status(HttpStatus.CREATED).body(savedBurger.id)
    }

    @DeleteMapping
    @Operation(
        responses = [
            ApiResponse(
                responseCode = "200",
                content = [Content(
                    schema = Schema(hidden = true)
                )]
            )
        ]
    )
    fun deleteBurger(
        @RequestParam(name = "burger_id") burgerId: Long
    ): ResponseEntity<Unit> {
        burgerService.deleteBurger(burgerId)
        return ResponseEntity.ok(Unit)
    }
}
