package com.example.amazing.repository

import com.example.amazing.models.Burger
import com.example.amazing.repositories.BurgerRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
class BurgerRepoTests {

    @Autowired
    private lateinit var burgerRepository: BurgerRepository

    @Test
    fun `Can insert a burger`() {
        val burger = Burger(
            vegetarian = false
        ).let {
            burgerRepository.save(it)
        }

        val queriedBurgers = burgerRepository.findAll()

        assertEquals(1, queriedBurgers.toList().size)
        assertEquals(burger, queriedBurgers.first())
    }

    @Test
    fun `Querying non existing burger`() {
        val nothing = burgerRepository.findByIdOrNull(42L)
        assertEquals(nothing, null)
    }

    @Test
    fun `Can delete a burger`() {
        val burger = Burger(
            vegetarian = false
        ).let {
            burgerRepository.save(it)
        }

        burgerRepository.deleteById(burger.id!!)

        val queriedBurgers = burgerRepository.findAll()

        assertEquals(0, queriedBurgers.toList().size)
    }

    @Test
    fun `Deleting non existing burger`() {
        try {
            burgerRepository.deleteById(42L)
            fail { "Should throw exception" }
        } catch (ex: EmptyResultDataAccessException) {
            assertEquals(ex.message, "No class com.example.amazing.models.Burger entity with id 42 exists!")
        }
    }

}
