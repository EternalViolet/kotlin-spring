package com.example.amazing.controller

import com.example.amazing.controllers.BurgerController
import com.example.amazing.models.Burger
import com.example.amazing.services.BurgerService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.json.JacksonTester
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@ExtendWith(SpringExtension::class)
@AutoConfigureJsonTesters
@WebMvcTest(BurgerController::class)
class BurgerControllerTests {

    @Autowired
    private lateinit var mvc: MockMvc

    @MockBean
    private lateinit var burgerService: BurgerService

    @Autowired
    private lateinit var jsonBurger: JacksonTester<Burger>

    @Autowired
    private lateinit var jsonBurgerList: JacksonTester<Iterable<Burger>>

    @Test
    fun `Can get one burger`() {
        val burger = Burger(3, false)

        given(burgerService.getBurger(3))
            .willReturn(burger)

        val response = mvc.perform(
            get("/burger")
                .queryParam("burger_id", "3")
                .accept(APPLICATION_JSON)
        ).andReturn().response

        assertEquals(HttpStatus.OK.value(), response.status)
        assertEquals(jsonBurgerList.write(listOf(burger)).json, response.contentAsString)
    }

    @Test
    fun `Can get all burgers`() {
        val burgers = listOf(
            Burger(1, true),
            Burger(2, false),
            Burger(3, false)
        )

        given(burgerService.getAllBurgers())
            .willReturn(burgers)

        val response = mvc.perform(
            get("/burger")
                .accept(APPLICATION_JSON)
        ).andReturn().response

        assertEquals(HttpStatus.OK.value(), response.status)
        assertEquals(jsonBurgerList.write(burgers).json, response.contentAsString)
    }

    @Test
    fun `Can create a burger`() {
        val burger = Burger(vegetarian = true)
        val burgerWithId = burger.copy(id = 1)

        given(burgerService.insertBurger(burger))
            .willReturn(burgerWithId)

        val response = mvc.perform(
            post("/burger")
                .contentType(APPLICATION_JSON)
                .content(jsonBurger.write(burger).json)
                .accept(APPLICATION_JSON)
        ).andReturn().response

        assertEquals(HttpStatus.CREATED.value(), response.status)
        assertEquals("1", response.contentAsString)
    }

    @Test
    fun `Can delete a burger`() {
        // When calling burgerService.delete it'll just mock itself, because it returns nothing.

        val response = mvc.perform(
            delete("/burger")
                .queryParam("burger_id", "3")
                .accept(APPLICATION_JSON)
        ).andReturn().response

        assertEquals(HttpStatus.OK.value(), response.status)
    }
}
