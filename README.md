# Kotlin Spring

Project for just a template with a nice structure to base future work off of.

Uses:
- Spring boot
- Kotlin
- JPA (using h2)

### Running

`./gradlew bootRun`

### Building + running

`./gradlew bootJar`

then find the jar in `/build/libs` and run via

`java -jar file.jar`

### Api docs
While running, navigate to `localhost:8080/swagger-ui` or find openapi json at `localhost:8080/v3/api-docs`.

### Testing

`./gradlew test`

#### References
[Spring Boot docs](https://docs.spring.io/autorepo/docs/spring-boot/current/reference/htmlsingle/)
